## Dataset

https://www.kaggle.com/arashnic/hr-analytics-job-change-of-data-scientists


```python

## for plotting
import matplotlib.pyplot as plt
import seaborn as sns
import itertools

## for statistical tests
import scipy

## for machine learning
from sklearn import preprocessing, impute, utils, linear_model, feature_selection, model_selection, metrics, ensemble
```


```python
from sklearn.datasets import load_boston

from sklearn.model_selection import train_test_split

from sklearn.linear_model import LinearRegression

import warnings
warnings.filterwarnings("ignore")

import math

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.ticker as mtick

import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor

## for plotting
import matplotlib.pyplot as plt
import seaborn as sns
import itertools


## for statistical tests
import scipy

## for machine learning
from sklearn import preprocessing, impute, utils, linear_model, feature_selection, model_selection, metrics, ensemble


%matplotlib inline
```


```python
dtf = pd.read_csv('aug_train.csv')
# test_df = pd.read_csv('aug_test.csv')
dtf.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>enrollee_id</th>
      <th>city</th>
      <th>city_development_index</th>
      <th>gender</th>
      <th>relevent_experience</th>
      <th>enrolled_university</th>
      <th>education_level</th>
      <th>major_discipline</th>
      <th>experience</th>
      <th>company_size</th>
      <th>company_type</th>
      <th>last_new_job</th>
      <th>training_hours</th>
      <th>target</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>8949</td>
      <td>city_103</td>
      <td>0.920</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>&gt;20</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1</td>
      <td>36</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>29725</td>
      <td>city_40</td>
      <td>0.776</td>
      <td>Male</td>
      <td>No relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>15</td>
      <td>50-99</td>
      <td>Pvt Ltd</td>
      <td>&gt;4</td>
      <td>47</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>11561</td>
      <td>city_21</td>
      <td>0.624</td>
      <td>NaN</td>
      <td>No relevent experience</td>
      <td>Full time course</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>5</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>never</td>
      <td>83</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>33241</td>
      <td>city_115</td>
      <td>0.789</td>
      <td>NaN</td>
      <td>No relevent experience</td>
      <td>NaN</td>
      <td>Graduate</td>
      <td>Business Degree</td>
      <td>&lt;1</td>
      <td>NaN</td>
      <td>Pvt Ltd</td>
      <td>never</td>
      <td>52</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>666</td>
      <td>city_162</td>
      <td>0.767</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>Masters</td>
      <td>STEM</td>
      <td>&gt;20</td>
      <td>50-99</td>
      <td>Funded Startup</td>
      <td>4</td>
      <td>8</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
dtf.drop(columns=['enrollee_id'], inplace=True)
```


```python
dtf = dtf.rename(columns={"target":"Y"})
```


```python
dtf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city</th>
      <th>city_development_index</th>
      <th>gender</th>
      <th>relevent_experience</th>
      <th>enrolled_university</th>
      <th>education_level</th>
      <th>major_discipline</th>
      <th>experience</th>
      <th>company_size</th>
      <th>company_type</th>
      <th>last_new_job</th>
      <th>training_hours</th>
      <th>Y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>city_103</td>
      <td>0.920</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>&gt;20</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1</td>
      <td>36</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>city_40</td>
      <td>0.776</td>
      <td>Male</td>
      <td>No relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>15</td>
      <td>50-99</td>
      <td>Pvt Ltd</td>
      <td>&gt;4</td>
      <td>47</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>city_21</td>
      <td>0.624</td>
      <td>NaN</td>
      <td>No relevent experience</td>
      <td>Full time course</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>5</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>never</td>
      <td>83</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>city_115</td>
      <td>0.789</td>
      <td>NaN</td>
      <td>No relevent experience</td>
      <td>NaN</td>
      <td>Graduate</td>
      <td>Business Degree</td>
      <td>&lt;1</td>
      <td>NaN</td>
      <td>Pvt Ltd</td>
      <td>never</td>
      <td>52</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>city_162</td>
      <td>0.767</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>Masters</td>
      <td>STEM</td>
      <td>&gt;20</td>
      <td>50-99</td>
      <td>Funded Startup</td>
      <td>4</td>
      <td>8</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>19153</th>
      <td>city_173</td>
      <td>0.878</td>
      <td>Male</td>
      <td>No relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>Humanities</td>
      <td>14</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1</td>
      <td>42</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>19154</th>
      <td>city_103</td>
      <td>0.920</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>14</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>4</td>
      <td>52</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>19155</th>
      <td>city_103</td>
      <td>0.920</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>Graduate</td>
      <td>STEM</td>
      <td>&gt;20</td>
      <td>50-99</td>
      <td>Pvt Ltd</td>
      <td>4</td>
      <td>44</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>19156</th>
      <td>city_65</td>
      <td>0.802</td>
      <td>Male</td>
      <td>Has relevent experience</td>
      <td>no_enrollment</td>
      <td>High School</td>
      <td>NaN</td>
      <td>&lt;1</td>
      <td>500-999</td>
      <td>Pvt Ltd</td>
      <td>2</td>
      <td>97</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>19157</th>
      <td>city_67</td>
      <td>0.855</td>
      <td>NaN</td>
      <td>No relevent experience</td>
      <td>no_enrollment</td>
      <td>Primary School</td>
      <td>NaN</td>
      <td>2</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1</td>
      <td>127</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>19158 rows × 13 columns</p>
</div>




```python
dtf.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 19158 entries, 0 to 19157
    Data columns (total 13 columns):
     #   Column                  Non-Null Count  Dtype  
    ---  ------                  --------------  -----  
     0   city                    19158 non-null  object 
     1   city_development_index  19158 non-null  float64
     2   gender                  14650 non-null  object 
     3   relevent_experience     19158 non-null  object 
     4   enrolled_university     18772 non-null  object 
     5   education_level         18698 non-null  object 
     6   major_discipline        16345 non-null  object 
     7   experience              19093 non-null  object 
     8   company_size            13220 non-null  object 
     9   company_type            13018 non-null  object 
     10  last_new_job            18735 non-null  object 
     11  training_hours          19158 non-null  int64  
     12  Y                       19158 non-null  float64
    dtypes: float64(2), int64(1), object(10)
    memory usage: 1.9+ MB
    


```python
'''
Recognize whether a column is numerical or categorical.
:parameter
    :param dtf: dataframe - input data
    :param col: str - name of the column to analyze
    :param max_cat: num - max number of unique values to recognize a column as categorical
:return
    "cat" if the column is categorical or "num" otherwise
'''
def utils_recognize_type(dtf, col, max_cat=20):
    if (dtf[col].dtype == "O") | (dtf[col].nunique() < max_cat):
        return "cat"
    else:
        return "num"
dic_cols = {col:utils_recognize_type(dtf, col, max_cat=20) for col in dtf.columns}
heatmap = dtf.isnull()
for k,v in dic_cols.items():
 if v == "num":
   heatmap[k] = heatmap[k].apply(lambda x: 0.5 if x is False else 1)
 else:
   heatmap[k] = heatmap[k].apply(lambda x: 0 if x is False else 1)
sns.heatmap(heatmap, cbar=False).set_title('Dataset Overview')
plt.show()
print("\033[1;37;40m Categerocial ", "\033[1;30;41m Numeric ", "\033[1;30;47m NaN ")
```


    
![png](images/output_9_0.png)
    


    [1;37;40m Categerocial  [1;30;41m Numeric  [1;30;47m NaN 
    


```python
fig = plt.figure(figsize=(20,15))
cols = 4
rows = math.ceil(float(dtf.shape[1]) / cols)
for i, column in enumerate(dtf.columns):
    ax = fig.add_subplot(rows, cols, i + 1)
    ax.set_title(column)
    if column == 'city':
        dtf[column].value_counts()[:20].plot(kind="bar", axes=ax)
    elif dtf.dtypes[column] == np.object:
        dtf[column].value_counts().plot(kind="bar", axes=ax)
    else:
        dtf[column].hist(axes=ax)
        plt.xticks(rotation="vertical")
plt.subplots_adjust(hspace=0.7, wspace=0.2)
```


    
![png](images/output_10_0.png)
    


## Target


```python
targets_counts = dtf['Y'].value_counts()
n_seekers_count = targets_counts[0]
seekers_count = targets_counts[1]
```


```python
category_names = ['Non Job-Seeker', 'Job-Seeker']
sizes = [n_seekers_count, seekers_count]
custom_colours = ['#ff7675','#74b9ff']

plt.figure(figsize=(1.5,1.5), dpi=227)
plt.pie(sizes, labels=category_names, textprops={'fontsize':5}, startangle=90,autopct='%1.1f%%',pctdistance=0.8,
        colors= custom_colours,)

# draw circle
center_circle = plt.Circle((0,0), radius=0.6, fc='white')
# plt.gca() to get current axis
plt.gca().add_artist(center_circle)

plt.show()
```


    
![png](images/output_13_0.png)
    


## Education Level

- Graduate
- Masters
- High School
- PhD
- Primary School


```python
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objs as go

el = dtf['education_level'].value_counts().reset_index()
el.columns = [
    'education_level', 
    'percent'
]
el['percent'] /= len(dtf)

fig = px.pie(
    el, 
    names='education_level', 
    values='percent', 
    title='Education_level', 
    width=800,
    height=500 
)

fig.show()


```python
et = dtf.sort_values(by='training_hours', ascending=True)[:7000]
figure = plt.figure(figsize=(10,6))
sns.boxenplot(y=et.education_level, x=et.training_hours)
plt.xticks()
plt.xlabel('training_hours')
plt.ylabel('education_level')
plt.title('education_level:training_hours ')
plt.show()


```


    
![png](images/output_17_0.png)


# City development index


```python
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objs as go

cd = dtf['city_development_index'].value_counts().reset_index()
cd.columns = [
    'city_development_index', 
    'count'
]
cd['city_development_index'] = cd['city_development_index'].astype(str) + '-'
cd = cd.sort_values(['count']).tail(50)

fig = px.area(
    cd, 
    x='count', 
    y='city_development_index', 
    orientation='h', 
    title='Count: City development index', 
    width=1000,
    height=900 
)

fig.show()


## City by city development index


```python
cdi = dtf.sort_values(by='city_development_index', ascending=True)[:3000]
figure = plt.figure(figsize=(10,6))
sns.barplot(y=cdi.city, x=cdi.city_development_index)
plt.xticks()
plt.xlabel('city_development_index')
plt.ylabel('city')
plt.title('City by city development index')
plt.show()
```


    
![png](images/output_21_0.png)
    



```python
f, axes = plt.subplots(1,1, figsize = (16, 5))
g1 = sns.distplot(dtf["city_development_index"], color="red",ax = axes)
plt.title("Distributional of city_development_index")
```




    Text(0.5, 1.0, 'Distributional of city_development_index')




    
![png](images/output_22_1.png)
    


## Experience


```python
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objs as go

ep = dtf['experience'].value_counts().reset_index()
ep.columns = [
    'experience', 
    'percent'
]
ep['percent'] /= len(dtf)

fig = px.pie(
    ep, 
    names='experience', 
    values='percent', 
    title='Experience', 
    width=800,
    height=500 
)

fig.show()


## Taining_hours


```python
f, axes = plt.subplots(1,1, figsize = (16, 5))
g1 = sns.distplot(dtf["training_hours"], color="green",ax = axes)
plt.title("Distributional of training_hours")
```




    Text(0.5, 1.0, 'Distributional of training_hours')




    
![png](images/output_26_1.png)
    


## Gender


```python
sns.countplot(y='gender', hue='Y', data=dtf,)
```




    <AxesSubplot:xlabel='count', ylabel='gender'>




    
![png](images/output_28_1.png)
    


## Relevent Experience


```python
sns.countplot(y='relevent_experience', hue='Y', data=dtf,)
```




    <AxesSubplot:xlabel='count', ylabel='relevent_experience'>




    
![png](images/output_30_1.png)
    


## Last New Job


```python
dtf['last_new_job'].value_counts().plot(kind='barh')
```




    <AxesSubplot:>




    
![png](images/output_32_1.png)
    


## Enrolled University


```python
dtf['enrolled_university'].value_counts().plot(kind='barh')
```




    <AxesSubplot:>




    
![png](images/output_34_1.png)
    


## Company Size


```python
dtf['company_size'].value_counts().plot(kind='barh')
```




    <AxesSubplot:>




    
![png](images/output_36_1.png)
    


## Experience


```python
dtf['experience'].value_counts().plot(kind='barh')
```




    <AxesSubplot:>




    
![png](images/output_38_1.png)
    


## Features & Target


```python
dtf['last_new_job'] = dtf['last_new_job'].apply(lambda x: 'Never' if x == 'never' else x) 
dtf['enrolled_university'][dtf['enrolled_university'] == 'no_enrollment'] = 'No Enrollment'
dtf['company_size'] = dtf['company_size'].apply(lambda x: '10-49' if x == '10/49' else x) 
dtf['experience'] = dtf['experience'].apply(lambda x: '0' if x == '<1' else x)
dtf['experience'] = dtf['experience'].apply(lambda x: '20' if x == '>20' else x)
dtf['relevent_experience'] = dtf['relevent_experience'].apply(lambda x: 1 if x == 'Has relevent experience' else 0)
```

## Missing Values


```python
pd.isnull(dtf).any() # Method 1
```




    city                      False
    city_development_index    False
    gender                     True
    relevent_experience       False
    enrolled_university        True
    education_level            True
    major_discipline           True
    experience                 True
    company_size               True
    company_type               True
    last_new_job               True
    training_hours            False
    Y                         False
    dtype: bool



### finding mean value for university enrollment


```python
enrolled_university = dtf[ dtf['Y'] == 1]['enrolled_university']
```


```python
enrolled_university = enrolled_university.apply(lambda x: 0 if x == 'No Enrollment' else 1 if x == 'Part time course' else 2)
enrolled_university.value_counts()
```




    0    2921
    2    1554
    1     302
    Name: enrolled_university, dtype: int64




```python
enrolled_university.mean()
```




    0.7138371362779987




```python
enrolled_university = dtf[ dtf['Y'] == 0]['enrolled_university']
```


```python
enrolled_university = enrolled_university.apply(lambda x: 0 if x == 'No Enrollment' else 1 if x == 'Part time course' else 2)
enrolled_university.value_counts()
```




    0    10896
    2     2589
    1      896
    Name: enrolled_university, dtype: int64




```python
enrolled_university.mean()
```




    0.42236283985814616



### finding mean value for company size


```python
company_size = dtf['company_size']
company_size.value_counts()
```




    50-99        3083
    100-500      2571
    10000+       2019
    10-49        1471
    1000-4999    1328
    <10          1308
    500-999       877
    5000-9999     563
    Name: company_size, dtype: int64




```python
def transform_company_size(x):
    if x == '<10':
        return 0
    elif x == '50-99':
        return 1
    elif x == '100-500':
        return 2
    elif x == '500-999':
        return 3
    elif x == '1000-4999':
        return 4
    elif x == '5000-9999':
        return 5
    elif x == '10000+':
        return 6
```


```python
company_size = dtf['company_size'].apply(transform_company_size)
company_size = company_size.astype(str).astype(float)
company_size.value_counts()
```




    1.0    3083
    2.0    2571
    6.0    2019
    4.0    1328
    0.0    1308
    3.0     877
    5.0     563
    Name: company_size, dtype: int64




```python
company_size.mean()
```




    2.6467784492297217




```python
dtf['company_size'] = company_size
```

### finding mean value for education level


```python
dtf['education_level'].value_counts()
```




    Graduate          11598
    Masters            4361
    High School        2017
    Phd                 414
    Primary School      308
    Name: education_level, dtype: int64




```python
def transform_education_level(x):
    if x == 'Primary School':
        return 0
    elif x == 'High School':
        return 1
    elif x == 'Graduate':
        return 2
    elif x == 'Masters':
        return 3
    elif x == 'Phd':
        return 4
```


```python
education_level = dtf['education_level'].apply(lambda x: transform_education_level(x))
```


```python
education_level.mean()
```




    2.136699112204514




```python
# dtf['education_level'].fillna(2,inplace=True)
```

### finding mean value for experience


```python
experience = dtf['experience'].apply(lambda x : '0' if x == '<1' else x )
experience = dtf['experience'].apply(lambda x : '20' if x == '>20' else x )
experience = dtf['experience'].astype(str).astype(float)
dtf['experience'] = experience
```


```python
experience.mean()
```




    9.92803645315037




```python
# dtf['experience'].fillna(10)
```


```python
# dtf['Y'].groupby(dtf['gender']).mean()
```


```python
dtf.isna().sum()/len(dtf)
```




    city                      0.000000
    city_development_index    0.000000
    gender                    0.235306
    relevent_experience       0.000000
    enrolled_university       0.020148
    education_level           0.024011
    major_discipline          0.146832
    experience                0.003393
    company_size              0.386731
    company_type              0.320493
    last_new_job              0.022080
    training_hours            0.000000
    Y                         0.000000
    dtype: float64



### finding mean value for education level


```python
dtf['last_new_job'].unique()
```




    array(['1', '>4', 'Never', '4', '3', '2', nan], dtype=object)




```python
last_new_job = dtf['last_new_job'].apply(lambda x : '0' if x == 'Never' else x )
last_new_job = last_new_job.apply(lambda x : '5' if x == '>4' else x )
last_new_job = last_new_job.astype(str).astype(float)
last_new_job
```




    0        1.0
    1        5.0
    2        0.0
    3        0.0
    4        4.0
            ... 
    19153    1.0
    19154    4.0
    19155    4.0
    19156    2.0
    19157    1.0
    Name: last_new_job, Length: 19158, dtype: float64




```python
dtf['last_new_job'] = last_new_job
```


```python
last_new_job.mean()
```




    2.0004270082732853




```python
# dtf['last_new_job'].fillna(2, inplace=True)
```


```python
dtf.dropna(inplace=True)
```


```python
dtf.isna().sum()/len(dtf)
```




    city                      0.0
    city_development_index    0.0
    gender                    0.0
    relevent_experience       0.0
    enrolled_university       0.0
    education_level           0.0
    major_discipline          0.0
    experience                0.0
    company_size              0.0
    company_type              0.0
    last_new_job              0.0
    training_hours            0.0
    Y                         0.0
    dtype: float64




```python
dtf['training_hours'] = dtf['training_hours'].astype(str).astype(int)
```


```python
# Fill nulls
dtf['company_type'].fillna('Unknown',inplace=True)
dtf['major_discipline'].fillna('Unknown',inplace=True)
dtf['gender'].fillna('Not provided',inplace=True)

# other
dtf['company_size'].fillna(3,inplace=True) ## fill with mean
```


```python
ed_order = ['Primary School','High School','Graduate','Masters','Phd']
enroll_order = ['No Enrollment','Part time course','Full time course']
disc_order = ['STEM','Unknown','Humanities','Other','Business Degree','Arts','No Major']
exp_yrs_order = ['<1','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','>20']
exp_yrs_order_2 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
size_order = ['0','<10', '10-49', '50-99', '100-500', '500-999', '1000-4999', '5000-9999', '10000+']
job_order = ['Never', '1', '2', '3', '4', '>4']
exp_order =['No relevant experience','Has relevant experience']
gender_order = ['Male','Female','Other','Not provided']
company_order = ['Pvt Ltd','Unknown','Funded Startup','Public Sector','Early Stage Startup','NGO','Other']
```


```python
background_color = "#fbfbfb"

fig = plt.figure(figsize=(22,15),dpi=150)
fig.patch.set_facecolor(background_color)
gs = fig.add_gridspec(3, 3)
gs.update(wspace=0.35, hspace=0.27)
ax0 = fig.add_subplot(gs[0, 0])
ax1 = fig.add_subplot(gs[0, 1])
ax2 = fig.add_subplot(gs[0, 2])
ax3 = fig.add_subplot(gs[1, 0])
ax4 = fig.add_subplot(gs[1, 1])
ax5 = fig.add_subplot(gs[1, 2])
ax6 = fig.add_subplot(gs[2, 0])
ax7 = fig.add_subplot(gs[2, 1])
ax8 = fig.add_subplot(gs[2, 2])


# Ax0 - EDUCATION LEVEL
train = pd.DataFrame(test_df["education_level"].value_counts())
train["Percentage"] = train["education_level"].apply(lambda x: x/sum(train["education_level"])*100)
train = train.sort_index()

test = pd.DataFrame(dtf["education_level"].value_counts())
test["Percentage"] = test["education_level"].apply(lambda x: x/sum(test["education_level"])*100)
test = test.sort_index()

ax0.bar(np.arange(len(train.index)), height=train["Percentage"], zorder=3, color="gray", width=0.05)
ax0.scatter(np.arange(len(train.index)), train["Percentage"], zorder=3,s=200, color="gray")
ax0.bar(np.arange(len(test.index))+0.4, height=test["Percentage"], zorder=3, color="#0e4f66", width=0.05)
ax0.scatter(np.arange(len(test.index))+0.4, test["Percentage"], zorder=3,s=200, color="#0e4f66")
ax0.text(-0.5, 68.5, 'Education Level', fontsize=14, fontweight='bold', fontfamily='serif', color="#323232")
ax0.yaxis.set_major_formatter(mtick.PercentFormatter())
ax0.yaxis.set_major_locator(mtick.MultipleLocator(10))
ax0.set_xticks(np.arange(len(train.index))+0.4 / 2)
ax0.set_xticklabels(list(train.index),rotation=0)

```




    [Text(0.2, 0, 'Graduate'),
     Text(1.2, 0, 'High School'),
     Text(2.2, 0, 'Masters'),
     Text(3.2, 0, 'Phd'),
     Text(4.2, 0, 'Primary School')]




    
![png](images/output_79_1.png)
    



```python
dtf.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 8004 entries, 1 to 19155
    Data columns (total 13 columns):
     #   Column                  Non-Null Count  Dtype  
    ---  ------                  --------------  -----  
     0   city                    8004 non-null   object 
     1   city_development_index  8004 non-null   float64
     2   gender                  8004 non-null   object 
     3   relevent_experience     8004 non-null   int64  
     4   enrolled_university     8004 non-null   object 
     5   education_level         8004 non-null   object 
     6   major_discipline        8004 non-null   object 
     7   experience              8004 non-null   float64
     8   company_size            8004 non-null   float64
     9   company_type            8004 non-null   object 
     10  last_new_job            8004 non-null   float64
     11  training_hours          8004 non-null   int32  
     12  Y                       8004 non-null   float64
    dtypes: float64(5), int32(1), int64(1), object(6)
    memory usage: 844.2+ KB
    


```python
dtf.corr() # Pearson Correlation Coefficients 
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city_development_index</th>
      <th>relevent_experience</th>
      <th>experience</th>
      <th>company_size</th>
      <th>last_new_job</th>
      <th>training_hours</th>
      <th>Y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>city_development_index</th>
      <td>1.000000</td>
      <td>-0.009884</td>
      <td>0.328431</td>
      <td>0.060952</td>
      <td>0.158087</td>
      <td>-0.009709</td>
      <td>-0.409786</td>
    </tr>
    <tr>
      <th>relevent_experience</th>
      <td>-0.009884</td>
      <td>1.000000</td>
      <td>0.129747</td>
      <td>-0.058548</td>
      <td>0.024449</td>
      <td>0.015048</td>
      <td>-0.033487</td>
    </tr>
    <tr>
      <th>experience</th>
      <td>0.328431</td>
      <td>0.129747</td>
      <td>1.000000</td>
      <td>0.078547</td>
      <td>0.414304</td>
      <td>-0.004597</td>
      <td>-0.185799</td>
    </tr>
    <tr>
      <th>company_size</th>
      <td>0.060952</td>
      <td>-0.058548</td>
      <td>0.078547</td>
      <td>1.000000</td>
      <td>0.097635</td>
      <td>-0.019352</td>
      <td>0.016552</td>
    </tr>
    <tr>
      <th>last_new_job</th>
      <td>0.158087</td>
      <td>0.024449</td>
      <td>0.414304</td>
      <td>0.097635</td>
      <td>1.000000</td>
      <td>-0.015305</td>
      <td>-0.065139</td>
    </tr>
    <tr>
      <th>training_hours</th>
      <td>-0.009709</td>
      <td>0.015048</td>
      <td>-0.004597</td>
      <td>-0.019352</td>
      <td>-0.015305</td>
      <td>1.000000</td>
      <td>-0.006345</td>
    </tr>
    <tr>
      <th>Y</th>
      <td>-0.409786</td>
      <td>-0.033487</td>
      <td>-0.185799</td>
      <td>0.016552</td>
      <td>-0.065139</td>
      <td>-0.006345</td>
      <td>1.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
mask = np.zeros_like(dtf.corr())

triangle_indecies = np.triu_indices_from(mask)
mask[triangle_indecies] = True
mask
```




    array([[1., 1., 1., 1., 1., 1., 1.],
           [0., 1., 1., 1., 1., 1., 1.],
           [0., 0., 1., 1., 1., 1., 1.],
           [0., 0., 0., 1., 1., 1., 1.],
           [0., 0., 0., 0., 1., 1., 1.],
           [0., 0., 0., 0., 0., 1., 1.],
           [0., 0., 0., 0., 0., 0., 1.]])




```python
plt.figure(figsize=(16,10))
sns.set_style('white')
sns.heatmap(dtf.corr(),mask=mask,annot=True,annot_kws={"size":14})
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.show()
```


    
![png](images/output_83_0.png)
    



```python
plt.scatter(x=dtf['training_hours'],y=dtf['Y'], alpha=0.1)
plt.show()
```


    
![png](images/output_84_0.png)
    



```python
# Extra libs

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier

from sklearn.pipeline import Pipeline
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
import xgboost as xgb
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics
from sklearn.model_selection import KFold, cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score, recall_score, roc_auc_score, precision_score
from sklearn.svm import LinearSVC
# from imblearn.over_sampling import BorderlineSMOTE
from numpy import where
```


```python
dtf.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 8004 entries, 1 to 19155
    Data columns (total 13 columns):
     #   Column                  Non-Null Count  Dtype  
    ---  ------                  --------------  -----  
     0   city                    8004 non-null   object 
     1   city_development_index  8004 non-null   float64
     2   gender                  8004 non-null   object 
     3   relevent_experience     8004 non-null   int64  
     4   enrolled_university     8004 non-null   object 
     5   education_level         8004 non-null   object 
     6   major_discipline        8004 non-null   object 
     7   experience              8004 non-null   float64
     8   company_size            8004 non-null   float64
     9   company_type            8004 non-null   object 
     10  last_new_job            8004 non-null   float64
     11  training_hours          8004 non-null   int32  
     12  Y                       8004 non-null   float64
    dtypes: float64(5), int32(1), int64(1), object(6)
    memory usage: 844.2+ KB
    


```python
dtf.education_level.unique()
```




    array(['Graduate', 'Masters', 'Phd'], dtype=object)




```python
aug_train.education_level.unique()
```




    array([0, 1, 2])




```python
dtf.major_discipline.unique()
```




    array(['STEM', 'Humanities', 'Business Degree', 'Other', 'No Major',
           'Arts'], dtype=object)




```python
dtf.company_type
```




    1               Pvt Ltd
    4        Funded Startup
    7               Pvt Ltd
    8               Pvt Ltd
    11              Pvt Ltd
                  ...      
    19146           Pvt Ltd
    19147           Pvt Ltd
    19149           Pvt Ltd
    19150     Public Sector
    19155           Pvt Ltd
    Name: company_type, Length: 8004, dtype: object




```python
list_of_columns = ['gender', 'enrolled_university', 'education_level',
                   'major_discipline', 'company_type']

aug_train = dtf

from sklearn.preprocessing import LabelEncoder

encoder = LabelEncoder()

for col in list_of_columns:
    aug_train[col] = encoder.fit_transform(dtf[col].astype(str))


# aug_train = aug_train.drop('count', 1)

# Final look at our df
aug_train.head(3)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city_development_index</th>
      <th>gender</th>
      <th>relevent_experience</th>
      <th>enrolled_university</th>
      <th>education_level</th>
      <th>major_discipline</th>
      <th>experience</th>
      <th>company_size</th>
      <th>company_type</th>
      <th>last_new_job</th>
      <th>training_hours</th>
      <th>Y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>0.776</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>15.0</td>
      <td>1.0</td>
      <td>5</td>
      <td>5.0</td>
      <td>47</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.767</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>20.0</td>
      <td>1.0</td>
      <td>1</td>
      <td>4.0</td>
      <td>8</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0.762</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>13.0</td>
      <td>0.0</td>
      <td>5</td>
      <td>5.0</td>
      <td>18</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
aug_train.sample(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city_development_index</th>
      <th>gender</th>
      <th>relevent_experience</th>
      <th>enrolled_university</th>
      <th>education_level</th>
      <th>major_discipline</th>
      <th>experience</th>
      <th>company_size</th>
      <th>company_type</th>
      <th>last_new_job</th>
      <th>training_hours</th>
      <th>Y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>9699</th>
      <td>0.939</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>20.0</td>
      <td>2.0</td>
      <td>2</td>
      <td>5.0</td>
      <td>82</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>18422</th>
      <td>0.624</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>10.0</td>
      <td>3.0</td>
      <td>4</td>
      <td>4.0</td>
      <td>33</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>11084</th>
      <td>0.926</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>17.0</td>
      <td>1.0</td>
      <td>5</td>
      <td>5.0</td>
      <td>114</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>7929</th>
      <td>0.920</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>20.0</td>
      <td>1.0</td>
      <td>5</td>
      <td>5.0</td>
      <td>154</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4254</th>
      <td>0.827</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>20.0</td>
      <td>6.0</td>
      <td>5</td>
      <td>5.0</td>
      <td>188</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>10717</th>
      <td>0.830</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>8.0</td>
      <td>1.0</td>
      <td>4</td>
      <td>3.0</td>
      <td>27</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>6997</th>
      <td>0.920</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>4.0</td>
      <td>3.0</td>
      <td>5</td>
      <td>1.0</td>
      <td>82</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2463</th>
      <td>0.920</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>15.0</td>
      <td>6.0</td>
      <td>5</td>
      <td>2.0</td>
      <td>320</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>15535</th>
      <td>0.920</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>5</td>
      <td>20.0</td>
      <td>2.0</td>
      <td>5</td>
      <td>1.0</td>
      <td>28</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>17940</th>
      <td>0.920</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>5.0</td>
      <td>5.0</td>
      <td>5</td>
      <td>2.0</td>
      <td>25</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
aug_train.drop(columns=['city'], inplace=True)
```


    ---------------------------------------------------------------------------

    KeyError                                  Traceback (most recent call last)

    Input In [460], in <cell line: 1>()
    ----> 1 aug_train.drop(columns=['city'], inplace=True)
    

    File ~\anaconda3\lib\site-packages\pandas\util\_decorators.py:311, in deprecate_nonkeyword_arguments.<locals>.decorate.<locals>.wrapper(*args, **kwargs)
        305 if len(args) > num_allow_args:
        306     warnings.warn(
        307         msg.format(arguments=arguments),
        308         FutureWarning,
        309         stacklevel=stacklevel,
        310     )
    --> 311 return func(*args, **kwargs)
    

    File ~\anaconda3\lib\site-packages\pandas\core\frame.py:4954, in DataFrame.drop(self, labels, axis, index, columns, level, inplace, errors)
       4806 @deprecate_nonkeyword_arguments(version=None, allowed_args=["self", "labels"])
       4807 def drop(
       4808     self,
       (...)
       4815     errors: str = "raise",
       4816 ):
       4817     """
       4818     Drop specified labels from rows or columns.
       4819 
       (...)
       4952             weight  1.0     0.8
       4953     """
    -> 4954     return super().drop(
       4955         labels=labels,
       4956         axis=axis,
       4957         index=index,
       4958         columns=columns,
       4959         level=level,
       4960         inplace=inplace,
       4961         errors=errors,
       4962     )
    

    File ~\anaconda3\lib\site-packages\pandas\core\generic.py:4267, in NDFrame.drop(self, labels, axis, index, columns, level, inplace, errors)
       4265 for axis, labels in axes.items():
       4266     if labels is not None:
    -> 4267         obj = obj._drop_axis(labels, axis, level=level, errors=errors)
       4269 if inplace:
       4270     self._update_inplace(obj)
    

    File ~\anaconda3\lib\site-packages\pandas\core\generic.py:4311, in NDFrame._drop_axis(self, labels, axis, level, errors, consolidate, only_slice)
       4309         new_axis = axis.drop(labels, level=level, errors=errors)
       4310     else:
    -> 4311         new_axis = axis.drop(labels, errors=errors)
       4312     indexer = axis.get_indexer(new_axis)
       4314 # Case for non-unique axis
       4315 else:
    

    File ~\anaconda3\lib\site-packages\pandas\core\indexes\base.py:6644, in Index.drop(self, labels, errors)
       6642 if mask.any():
       6643     if errors != "ignore":
    -> 6644         raise KeyError(f"{list(labels[mask])} not found in axis")
       6645     indexer = indexer[~mask]
       6646 return self.delete(indexer)
    

    KeyError: "['city'] not found in axis"



```python
X = aug_train.dropna().drop(columns=['Y'])
y = aug_train.dropna()['Y']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=80)
```


```python
aug_train.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 8004 entries, 1 to 19155
    Data columns (total 12 columns):
     #   Column                  Non-Null Count  Dtype  
    ---  ------                  --------------  -----  
     0   city_development_index  8004 non-null   float64
     1   gender                  8004 non-null   int32  
     2   relevent_experience     8004 non-null   int64  
     3   enrolled_university     8004 non-null   int32  
     4   education_level         8004 non-null   int32  
     5   major_discipline        8004 non-null   int32  
     6   experience              8004 non-null   float64
     7   company_size            8004 non-null   float64
     8   company_type            8004 non-null   int32  
     9   last_new_job            8004 non-null   float64
     10  training_hours          8004 non-null   int32  
     11  Y                       8004 non-null   float64
    dtypes: float64(5), int32(6), int64(1)
    memory usage: 625.3 KB
    


```python

# # Classification with a linear SVM
# svc = LinearSVC(dual=False, random_state=123)
# params_grid = {"C": [10 ** k for k in range(-3, 4)]}
# clf = GridSearchCV(svc, params_grid)
# clf.fit(X_train, y_train)
# print(
#     "Accuracy on the test set with raw data: {:.3f}".format(clf.score(X_test, y_test))
# )

# print(clf.best_params_)
```


```python
models_fscore = {}
```


```python
clf_all = DecisionTreeClassifier()
clf_all.fit(X_train, y_train)
```




    DecisionTreeClassifier()




```python
clf_all.score(X_test, y_test)
```




    0.7747710241465445




```python
#طبعنا أهم الفيتشرز
print(clf_all.feature_importances_ * 10)
import numpy as np
n_features = X_train.shape[1]
plt.barh(range(n_features), clf_all.feature_importances_[:n_features], align='center')
plt.yticks(np.arange(n_features), X_train.columns[:n_features] )
plt.xlabel('Feature Importance')
plt.ylabel('Feature')
plt.show()
```

    [3.23253748 0.19378716 0.22327641 0.30988715 0.27033486 0.28278876
     1.02382443 0.79820251 0.36573799 0.49970431 2.79991894]
    


    
![png](images/output_100_1.png)
    



```python
from sklearn.ensemble import RandomForestClassifier
```


```python
rtc = RandomForestClassifier()
rtc.fit(X_train, y_train)
```




    RandomForestClassifier()




```python
#طبعنا أهم الفيتشرز
print(rtc.feature_importances_ * 10)
import numpy as np
n_features = X_train.shape[1]
plt.barh(range(n_features), rtc.feature_importances_[:n_features], align='center')
plt.yticks(np.arange(n_features), X_train.columns[:n_features] )
plt.xlabel('Feature Importance')
plt.ylabel('Feature')
plt.show()
```

    [3.05036301 0.20279912 0.17975007 0.25580505 0.3151457  0.18468507
     1.39373981 0.89429894 0.37312609 0.68580478 2.46448236]
    


    
![png](images/output_103_1.png)
    



```python
rtc.score(X_test, y_test)
```




    0.8451290591174022




```python
mask = np.zeros_like(dtf.corr())

triangle_indecies = np.triu_indices_from(mask)
mask[triangle_indecies] = True
mask
```




    array([[1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
           [0., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
           [0., 0., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
           [0., 0., 0., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
           [0., 0., 0., 0., 1., 1., 1., 1., 1., 1., 1., 1.],
           [0., 0., 0., 0., 0., 1., 1., 1., 1., 1., 1., 1.],
           [0., 0., 0., 0., 0., 0., 1., 1., 1., 1., 1., 1.],
           [0., 0., 0., 0., 0., 0., 0., 1., 1., 1., 1., 1.],
           [0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 1., 1.],
           [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 1.],
           [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1.],
           [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]])




```python
plt.figure(figsize=(16,10))
sns.set_style('white')
sns.heatmap(dtf.corr(),mask=mask,annot=True,annot_kws={"size":14})
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.show()
```


    
![png](images/output_106_0.png)
    



```python
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.metrics import classification_report


def train_and_test(classifier, X_train, X_test, y_train, y_test, name):
    
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    roc_auc = roc_auc_score(y_test, y_pred)
#     roc_auc = classifier.decision_function(X_test)
    fpr, tpr, _ = roc_curve(y_test, y_pred)
    classification_rep = classification_report(y_test, y_pred)
    con_matrix = confusion_matrix(y_test, y_pred)

    # ploting confustion matrix
    plt.figure(figsize=(12,12))
    plt.subplot(2,1,1)
    sns.heatmap(con_matrix, annot=True, fmt="d", xticklabels=['0', '1'], yticklabels=['0', '1'])
    plt.ylabel("Real value")
    plt.xlabel("Predicted value")
    plt.show()

    # print scores
    print ("accuracy  score: {} %".format(accuracy))
    print ("auc  score: {} ".format(roc_auc))
    print(classification_rep)
    
    print ("Train set accurecy: {:.2f} ".format(tree_clf.score(X_train_l1,y_train)))
    print ("Test set accurecy: {:.2f} ".format(tree_clf.score(X_test_l1,y_test)))

    print('Accuracy: {:.2f}'.format(accuracy_score(y_test, y_pred)))
    print('Precision: {:.2f}'.format(precision_score(y_test, y_pred)))
    print('Recall: {:.2f}'.format(recall_score(y_test, y_pred)))
    print('F1: {:.2f}'.format(f1_score(y_test, y_pred)))
    models_fscore[name] = f1_score(y_test, y_pred)

    # print ROC curve
    plt.figure()
    plt.plot(fpr, tpr, color='darkorange',
         lw=2, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()
    
    return f1_score(y_test, y_pred)
```


```python
X.shape
```




    (8004, 11)




```python
y.shape
```




    (8004,)




```python
from sklearn.feature_selection import RFE
### your code here
rfe = RFE(estimator=DecisionTreeClassifier(), n_features_to_select= 7)

select = rfe.fit(X_train,y_train)
# X_train_l1, X_test_l1, y_train, y_test = train_test_split(fea)

X_train_l1 = select.transform(X_train)
X_test_l1 = select.transform(X_test)
```


```python
select.get_feature_names_out()
```




    array(['city_development_index', 'enrolled_university', 'education_level',
           'experience', 'company_size', 'last_new_job', 'training_hours'],
          dtype=object)




```python
#بعمل اوبجيكت من مصنف شجرة القرار
tree_clf = RandomForestClassifier(random_state=42) #max_depth: ارتفاع (عمق الشجرة)
#random_state=42 لتثبيت الخرج
tree_clf.fit(X_train_l1, y_train) #درّب 
```




    RandomForestClassifier(random_state=42)




```python
from sklearn.metrics import f1_score

y_pred = tree_clf.predict(X_test_l1)
print ("Train set accurecy: {:.2f} ".format(tree_clf.score(X_train_l1,y_train)))
print ("Test set accurecy: {:.2f} ".format(tree_clf.score(X_test_l1,y_test)))

print('Accuracy: {:.2f}'.format(accuracy_score(y_test, y_pred)))
print('Precision: {:.2f}'.format(precision_score(y_test, y_pred)))
print('Recall: {:.2f}'.format(recall_score(y_test, y_pred)))
print('F1: {:.2f}'.format(f1_score(y_test, y_pred)))

```

    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.85
    Precision: 0.53
    Recall: 0.31
    F1: 0.39
    


```python
train_and_test(DecisionTreeClassifier(random_state=42), X_train_l1, X_test_l1, y_train, y_test, 'DecisionTreeClassifier')
```


    
![png](images/output_114_0.png)
    


    accuracy  score: 0.7789342214820982 %
    auc  score: 0.603220413664403 
                  precision    recall  f1-score   support
    
             0.0       0.87      0.86      0.87      2020
             1.0       0.32      0.35      0.33       382
    
        accuracy                           0.78      2402
       macro avg       0.60      0.60      0.60      2402
    weighted avg       0.79      0.78      0.78      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.78
    Precision: 0.32
    Recall: 0.35
    F1: 0.33
    


    
![png](images/output_114_2.png)
    





    0.3320754716981132




```python
train_and_test(RandomForestClassifier(random_state=42,max_leaf_nodes=30), X_train_l1, X_test_l1, y_train, y_test, 'RandomForestClassifier')
```


    
![png](images/output_115_0.png)
    


    accuracy  score: 0.8488759367194005 %
    auc  score: 0.6331294385983101 
                  precision    recall  f1-score   support
    
             0.0       0.88      0.95      0.91      2020
             1.0       0.54      0.32      0.40       382
    
        accuracy                           0.85      2402
       macro avg       0.71      0.63      0.66      2402
    weighted avg       0.83      0.85      0.83      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.85
    Precision: 0.54
    Recall: 0.32
    F1: 0.40
    


    
![png](images/output_115_2.png)
    





    0.39999999999999997




```python
score = train_and_test(LogisticRegression(random_state=42), X_train_l1, X_test_l1, y_train, y_test , 'LogisticRegression')
score
```


    
![png](images/output_116_0.png)
    


    accuracy  score: 0.8434637801831807 %
    auc  score: 0.570474573635374 
                  precision    recall  f1-score   support
    
             0.0       0.86      0.97      0.91      2020
             1.0       0.52      0.17      0.26       382
    
        accuracy                           0.84      2402
       macro avg       0.69      0.57      0.58      2402
    weighted avg       0.81      0.84      0.81      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.84
    Precision: 0.52
    Recall: 0.17
    F1: 0.26
    


    
![png](images/output_116_2.png)
    





    0.2569169960474308




```python
score = train_and_test(KNeighborsClassifier(), X_train_l1, X_test_l1, y_train, y_test, 'KNeighborsClassifier')
score
```


    
![png](images/output_117_0.png)
    


    accuracy  score: 0.8243130724396336 %
    auc  score: 0.5293699134311337 
                  precision    recall  f1-score   support
    
             0.0       0.85      0.96      0.90      2020
             1.0       0.32      0.10      0.15       382
    
        accuracy                           0.82      2402
       macro avg       0.59      0.53      0.53      2402
    weighted avg       0.77      0.82      0.78      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.82
    Precision: 0.32
    Recall: 0.10
    F1: 0.15
    


    
![png](images/output_117_2.png)
    





    0.14919354838709678




```python

# from sklearn.model_selection import learning_curve


# N, train_lc, val_lc = learning_curve(DecisionTreeClassifier(),#الموديل
#                                          X_train, y_train, cv=7,
# #                                          train_sizes= np.linspace(0.3, 1, 25)
#                                     )#عم اقسم الداتا من تلتا ل كلها

# ax[i].plot(N, np.mean(train_lc, 1), color='blue', label='training score')
# ax[i].plot(N, np.mean(val_lc, 1), color='red', label='validation score')
# ax[i].hlines(np.mean([train_lc[-1], val_lc[-1]]), N[0], N[-1],
#              color='gray', linestyle='dashed')

# ax[i].set_ylim(0, 1)
# ax[i].set_xlim(N[0], N[-1])
# ax[i].set_xlabel('training size')
# ax[i].set_ylabel('score')
# ax[i].set_title('degree = {0}'.format(degree), size=14)
# ax[i].legend(loc='best')



train_sizes, train_scores, test_scores, fit_times, _ = learning_curve(best_model, X_train, y_train, cv=30,return_times=True)

plt.plot(train_sizes,np.mean(train_scores,axis=1))
```




    [<matplotlib.lines.Line2D at 0x1bb96e5d970>]




    
![png](images/output_118_1.png)
    



```python
score = train_and_test(AdaBoostClassifier(), X_train_l1, X_test_l1, y_train, y_test, 'AdaBoostClassifier')
score
```


    
![png](images/output_119_0.png)
    


    accuracy  score: 0.858451290591174 %
    auc  score: 0.7035664299414235 
                  precision    recall  f1-score   support
    
             0.0       0.90      0.93      0.92      2020
             1.0       0.57      0.48      0.52       382
    
        accuracy                           0.86      2402
       macro avg       0.73      0.70      0.72      2402
    weighted avg       0.85      0.86      0.85      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.86
    Precision: 0.57
    Recall: 0.48
    F1: 0.52
    


    
![png](images/output_119_2.png)
    





    0.5170454545454545




```python
from sklearn.dummy import DummyClassifier

score = train_and_test(DummyClassifier(), X_train_l1, X_test_l1, y_train, y_test, 'DummyClassifier')
score
```


    
![png](images/output_120_0.png)
    


    accuracy  score: 0.8409658617818485 %
    auc  score: 0.5 
                  precision    recall  f1-score   support
    
             0.0       0.84      1.00      0.91      2020
             1.0       0.00      0.00      0.00       382
    
        accuracy                           0.84      2402
       macro avg       0.42      0.50      0.46      2402
    weighted avg       0.71      0.84      0.77      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.84
    Precision: 0.00
    Recall: 0.00
    F1: 0.00
    


    
![png](images/output_120_2.png)
    





    0.0




```python
score = train_and_test(GradientBoostingClassifier(), X_train_l1, X_test_l1, y_train, y_test)
score
```


    
![png](images/output_121_0.png)
    


    accuracy  score: 0.8530391340549542 %
    auc  score: 0.6748755896532062 
                  precision    recall  f1-score   support
    
             0.0       0.89      0.94      0.91      2020
             1.0       0.55      0.41      0.47       382
    
        accuracy                           0.85      2402
       macro avg       0.72      0.67      0.69      2402
    weighted avg       0.84      0.85      0.84      2402
    
    Train set accurecy: 0.87 
    Test set accurecy: 0.86 
    Accuracy: 0.85
    Precision: 0.55
    Recall: 0.41
    F1: 0.47
    


    
![png](images/output_121_2.png)
    





    0.47234678624813153




```python
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures, MinMaxScaler

pipline_model = make_pipeline(RFE(estimator=DecisionTreeClassifier(), n_features_to_select= 8),
                              StandardScaler(),
                              PolynomialFeatures(degree=2),
                              AdaBoostClassifier())

pipline_model.get_params
```




    <bound method Pipeline.get_params of Pipeline(steps=[('rfe',
                     RFE(estimator=LogisticRegression(), n_features_to_select=8)),
                    ('standardscaler', StandardScaler()),
                    ('polynomialfeatures', PolynomialFeatures()),
                    ('adaboostclassifier', AdaBoostClassifier())])>




```python

param_grid = {'rfe__n_features_to_select': np.arange(4,9),
              'polynomialfeatures__degree': np.arange(6,9)}

grid = GridSearchCV(pipline_model, param_grid,)
```


```python
grid.fit(X_train, y_train)
```




    GridSearchCV(estimator=Pipeline(steps=[('rfe',
                                            RFE(estimator=LogisticRegression(),
                                                n_features_to_select=8)),
                                           ('standardscaler', StandardScaler()),
                                           ('polynomialfeatures',
                                            PolynomialFeatures()),
                                           ('adaboostclassifier',
                                            AdaBoostClassifier())]),
                 param_grid={'polynomialfeatures__degree': array([6, 7, 8]),
                             'rfe__n_features_to_select': array([4, 5, 6, 7, 8])})




```python
grid.best_params_
```




    {'polynomialfeatures__degree': 6, 'rfe__n_features_to_select': 5}




```python
grid.best_score_
```




    0.8605257435282055




```python
best_model = make_pipeline(RFE(estimator=DecisionTreeClassifier(), n_features_to_select= 5),
                              PolynomialFeatures(degree=2),
                              AdaBoostClassifier(),)
```


```python
train_and_test(best_model, X_train, X_test, y_train, y_test,'RFE->PolynomialFeatures->AdaBoostClassifier')
```


    
![png](images/output_128_0.png)
    


    accuracy  score: 0.8572023313905079 %
    auc  score: 0.6985783525996578 
                  precision    recall  f1-score   support
    
             0.0       0.90      0.93      0.92      2020
             1.0       0.56      0.47      0.51       382
    
        accuracy                           0.86      2402
       macro avg       0.73      0.70      0.71      2402
    weighted avg       0.85      0.86      0.85      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.86
    Precision: 0.56
    Recall: 0.47
    F1: 0.51
    


    
![png](images/output_128_2.png)
    





    0.5092989985693849




```python

pipline_model2 = make_pipeline(RFE(estimator=LogisticRegression(), n_features_to_select= 8),
                              PolynomialFeatures(degree=2),
                              AdaBoostClassifier())

pipline_model.get_params
```




    <bound method Pipeline.get_params of Pipeline(steps=[('rfe',
                     RFE(estimator=LogisticRegression(), n_features_to_select=8)),
                    ('standardscaler', StandardScaler()),
                    ('polynomialfeatures', PolynomialFeatures()),
                    ('adaboostclassifier', AdaBoostClassifier())])>




```python

param_grid = {'rfe__n_features_to_select': np.arange(4,9),
              'polynomialfeatures__degree': np.arange(2,7)}

grid = GridSearchCV(pipline_model2, param_grid,)
```


```python
grid.fit(X_train, y_train)
```




    GridSearchCV(estimator=Pipeline(steps=[('rfe',
                                            RFE(estimator=LogisticRegression(),
                                                n_features_to_select=8)),
                                           ('polynomialfeatures',
                                            PolynomialFeatures()),
                                           ('adaboostclassifier',
                                            AdaBoostClassifier())]),
                 param_grid={'polynomialfeatures__degree': array([2, 3, 4, 5, 6]),
                             'rfe__n_features_to_select': array([4, 5, 6, 7, 8])})




```python
grid.best_params_
```




    {'polynomialfeatures__degree': 2, 'rfe__n_features_to_select': 5}




```python

best_model2 = make_pipeline(RFE(estimator=LogisticRegression(), n_features_to_select= 5),
                              PolynomialFeatures(degree=2),
                              AdaBoostClassifier())
```


```python
train_and_test(best_model2, X_train, X_test, y_train, y_test, 'RFE->PolynomialFeatures->AdaBoostClassifier')
```


    
![png](images/output_134_0.png)
    


    accuracy  score: 0.8592839300582847 %
    auc  score: 0.7178593644704785 
                  precision    recall  f1-score   support
    
             0.0       0.91      0.93      0.92      2020
             1.0       0.56      0.51      0.54       382
    
        accuracy                           0.86      2402
       macro avg       0.74      0.72      0.73      2402
    weighted avg       0.85      0.86      0.86      2402
    
    Train set accurecy: 1.00 
    Test set accurecy: 0.85 
    Accuracy: 0.86
    Precision: 0.56
    Recall: 0.51
    F1: 0.54
    


    
![png](images/output_134_2.png)
    





    0.5357142857142857




```python
pd.DataFrame(models_fscore,index = ['a','b']).transpose()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a</th>
      <th>b</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>DecisionTreeClassifier</th>
      <td>0.332075</td>
      <td>0.332075</td>
    </tr>
    <tr>
      <th>RandomForestClassifier</th>
      <td>0.400000</td>
      <td>0.400000</td>
    </tr>
    <tr>
      <th>LogisticRegression</th>
      <td>0.256917</td>
      <td>0.256917</td>
    </tr>
    <tr>
      <th>KNeighborsClassifier</th>
      <td>0.149194</td>
      <td>0.149194</td>
    </tr>
    <tr>
      <th>AdaBoostClassifier</th>
      <td>0.517045</td>
      <td>0.517045</td>
    </tr>
    <tr>
      <th>DummyClassifier</th>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>RFE-&gt;PolynomialFeatures-&gt;AdaBoostClassifier</th>
      <td>0.535714</td>
      <td>0.535714</td>
    </tr>
  </tbody>
</table>
</div>




```python

```


```python

```
